<?php

namespace App\Auth;
include("../../vendor/autoload.php");
use App\Utility\Utility;
use PDO;

session_start();

class Auth
{
    private $username;
    private $email;
    private $password;

    public function setData($data = '')
    {
        $this->username = $data['username'];
        $this->email = $data['email'];
        $this->password = $data['password'];
        return $this;
    }

    public function store()
    {
//        echo $this->username . $this->password . $this->email;
//        die();
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=php48', 'root', '');
            $query = "INSERT INTO users(id, username, password, email)   VALUES(:i, :u, :p, :e)";
            $stmt = $pdo->prepare($query);

            $status = $stmt->execute(
                array(
                    ':i' => null,
                    ':u' => $this->username,
                    ':p' => $this->password,
                    ':e' => $this->email,
                )
            );
            if ($status) {
                $_SESSION['Message'] = "<h1>Welcome..Successfully Registerd</h1>";
                header('location:create.php');
            } else {
                echo "Something Going Wrong";
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

}